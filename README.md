# cv-builder-demo

This project demonstrates the solution I made for keeping my CV up-to-date.
The Python script populates the template with the contents from the `.yaml` file and writes the output to a new file.

The main benefit is that I can worry about writing the content separatedly from messing with the layouts.
Otherwise it makes sense to track the changes with git and make branches for customized CV's.

Purpose for making this public is to share information and show how I do things.


While possible I wouldn't expect anyone to find use for this without custom modifications as some aspects require manual edits.

For now I won't be developing this further publicly but in the ideal world you could use it in the same way as other CV building services.

## Usage

Steps would be simple as:

1. Customize template

2. Edit textual content

3. Run Python script

4. Open and save as PDF from Libre Office Writer

`.fodt is` "flat XML ODF text document" which is what Libre Office writer uses but it's not compressed which means git and humans can read it.

So you can edit the template using Libre Office Writer but I would recommend opening it with VScode or similar to manually edit the XML contents.

YAML should be quite intuitive to edit. However, the tags need to exactly match the placeholders in the template.

To run the Python script:

```sh
python -m venv cv_venv

source ./cv_venv/bin/activate #For Linux

python fill_template.py [<en (default), fi>] [<output_file.fodt>]
```
