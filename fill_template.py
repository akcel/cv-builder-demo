import sys
import yaml
from os.path import isfile


INPUT_TEMPLATE_PATH = "CV_template.fodt"
INPUT_CONTENT_PATH = "CV_source.yaml"
OUTPUT_FILE_PATH = "CV_filler_output.fodt"

language = "en"


def loadInputContent():
    sourceContent = {}
    with open(INPUT_CONTENT_PATH, 'r') as CV_source_file:
        sourceContent = yaml.safe_load(CV_source_file)["FILL_WITH"]
    return sourceContent

def loadTemplate():
    cvAsString = ""
    with open(INPUT_TEMPLATE_PATH, 'r') as CV_template_file:
        cvAsString = CV_template_file.read()
    return cvAsString


def fill_placeholders(content, template):
    # Profile section
    cvAsString = template.replace("FILL_WITH_profile_description", content["profile_description"][language])

    # Work experience section
    cvAsString = cvAsString.replace("FILL_WITH_jobs_heading", content["jobs"]["jobs_heading"][language])
    for job in content["jobs"]:
        if job == "jobs_heading":
            continue
        for field in content["jobs"][job]:
            cvAsString = cvAsString.replace(f"FILL_WITH_{job}_{field}", content["jobs"][job][field][language])

    # Rest of the sections
    for section in ["education", "skills", "hobbies"]:
        for field in content[section]:
            cvAsString = cvAsString.replace(f"FILL_WITH_{field}", content[section][field][language])
    return cvAsString


def writeOutput(content):
    # Write output file
    with open(OUTPUT_FILE_PATH, 'w') as outputFile:
        outputFile.write(content)


def __main__():
    sourceContent = loadInputContent()
    cvAsString = loadTemplate()

    cvAsString = fill_placeholders(sourceContent, cvAsString)

    writeOutput(cvAsString)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        # No additional arguments
        print(f"Usage: python {sys.argv[0]} [<en (default), fi>] [<output_file.fodt>]")
        print("Using defaults.")
    else:
        language = sys.argv[1]
        if language not in ("fi", "en"):
            sys.exit("Use \"fi\" or \"en\"")
        if len(sys.argv) == 3:
            OUTPUT_FILE_PATH == sys.argv[2]
    # Add language to filepath
    OUTPUT_FILE_PATH = OUTPUT_FILE_PATH.replace(".fodt", f"_{language}.fodt")

    if isfile(INPUT_CONTENT_PATH):
        if isfile(INPUT_TEMPLATE_PATH):
            __main__()
            print(f"Template filled with provided content to \"{OUTPUT_FILE_PATH}\" ")
        else:
            print(f"file \"{INPUT_TEMPLATE_PATH}\" was not found")
    else:
        print(f"file \"{INPUT_CONTENT_PATH}\" was not found")
